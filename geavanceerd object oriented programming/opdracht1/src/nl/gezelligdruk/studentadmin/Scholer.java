package nl.gezelligdruk.studentadmin;

class Scholer extends Student {

    //private final CPP cpp;
    private int modules = 0;
    private final CPP cpp;

    Scholer(String naam, CPP cpp) {
        super(naam);
        this.cpp = cpp;
    }

    private int getModules() {
        return modules;
    }

    /**
     * verhoog aantal behaalde modules met 1
     *
     */
    void setModules() {
        modules++;
    }

    /**
     * Retourneer alle informatie van een student
     *
     * @return string met alle informatie van een student
     */
    protected String getInformation() {
        return getNaam() + ", " + cpp.getNaam() + ", " + getModules() + " modules, " + (isGeslaagd() ? " geslaagd" : "niet geslaagd");
    }

    /**
     * Is een student geslaagd of niet, afhankelijk of het aantal behaalde modules groter of gelijk is aan het aantal
     * benodigde modules van de ccp van de student.
     *
     * @return true als student geslaagd is
     */
    private Boolean isGeslaagd() {
        boolean geslaagd = false;
        if (modules >= cpp.getAantalmodules()) {
            geslaagd = true;
        }
        return geslaagd;
    }
}
