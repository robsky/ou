package nl.gezelligdruk.studentadmin;

class ReguliereStudent extends Student {

    private final Opleiding opleiding;
    private double behaaldePunten = 0.0;

    ReguliereStudent(String naam, Opleiding opleiding) {
        super(naam);
        this.opleiding = opleiding;
    }

    /**
     * Test of student geslaagd is voor opleiding
     *
     * @return True als geslaagd
     */
    private Boolean isGeslaagd() {
        boolean geslaagd = false;
        if (behaaldePunten >= opleiding.getStudiePunten()) {
            geslaagd = true;
        }
        return geslaagd;
    }

    private double getBehaaldePunten() {
        return behaaldePunten;
    }

    /**
     * Verhoog aantal behaalde punten
     *
     * @param behaaldePunten aantal waarmee het aantal behaalde punten verhoogt wordt
     */
    public void setBehaaldePunten(double behaaldePunten) {
        this.behaaldePunten += behaaldePunten;
    }

    protected String getInformation() {
        return getNaam() + ", " + opleiding.getNaam() + ", " + getBehaaldePunten() + " behaalde punten, " + (isGeslaagd() ? "geslaagd" : "niet geslaagd");
    }
}
