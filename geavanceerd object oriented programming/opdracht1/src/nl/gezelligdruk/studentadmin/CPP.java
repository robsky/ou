package nl.gezelligdruk.studentadmin;

class CPP extends Programma {

    private final Integer aantalmodules;

    CPP(String naam, int aantalModules) {
        super(naam);
        this.aantalmodules = aantalModules;
    }

    Integer getAantalmodules() {
        return aantalmodules;
    }

}