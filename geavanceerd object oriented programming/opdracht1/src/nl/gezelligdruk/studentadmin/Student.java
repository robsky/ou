package nl.gezelligdruk.studentadmin;

abstract class Student {

    private final String naam;
    // --Commented out by Inspection (2019-05-12 23:00):private Boolean geslaagd = false;

    Student(String naam) {
        this.naam = naam;
    }

    String getNaam() {
        return naam;
    }

    protected abstract String getInformation();

    // --Commented out by Inspection (2019-05-12 23:00):protected abstract Boolean isGeslaagd();

    @Override
    public String toString() {

        return naam;
    }
}
