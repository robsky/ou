package nl.gezelligdruk.studentadmin;

class Opleiding extends Programma {

    private final double studiePunten;

    Opleiding(String naam, double studiePunten) {
        super(naam);
        this.studiePunten = studiePunten;
    }

    double getStudiePunten() {
        return studiePunten;
    }
}
