package nl.gezelligdruk.studentadmin;

import java.util.ArrayList;
import java.util.Objects;

public class StudentAdmin {
    private final ArrayList<Student> studenten;
    private final ArrayList<Programma> programmas;

    public StudentAdmin() {
        this.studenten = new ArrayList<>();
        this.programmas = new ArrayList<>();

        Programma wiskunde = new Opleiding("Wiskunde", 160);
        Programma informatica = new Opleiding("Informatica", 200);
        Programma java = new CPP("Java", 6);
        Programma softwareArchitect = new CPP("Softwarearchitect", 4);
        Programma systeemOntwikkelaar = new CPP("Systeemontwikkelaar", 3);

        programmas.add(wiskunde);
        programmas.add(informatica);
        programmas.add(java);
        programmas.add(softwareArchitect);
        programmas.add(systeemOntwikkelaar);
    }

    /**
     * @return lijst van ccp's
     */
    public ArrayList<String> getCpps() {
        ArrayList<String> cpps = new ArrayList<>();
        for (Programma programma : programmas) {
            if (programma instanceof CPP) {
                cpps.add(programma.toString());
            }
        }
        return cpps;
    }

    /**
     * lijst van opleidingen
     *
     * @return lijst van opleidingen
     */
    public ArrayList<String> getOpleidingen() {
        ArrayList<String> opleidingen = new ArrayList<>();
        for (Programma programma : programmas) {
            if (programma instanceof Opleiding) {
                opleidingen.add(programma.toString());
            }
        }
        return opleidingen;
    }

    //TODO: getCpps en getOpleidingen moet generiek gemaakt kunnen worden???


// --Commented out by Inspection START (2019-05-12 23:04):
//    public ArrayList<Student> getStudenten() {
//        return studenten;
//    }
// --Commented out by Inspection STOP (2019-05-12 23:04)

    /**
     * Zoek een programma op basis van de naam.
     *
     * @param naam naam van het programma
     * @return gevonden programma ({@link CPP} of {@link Opleiding})
     */
    private Programma getProgramma(String naam) {
        Programma programmaGevonden = null;
        for (Programma programma : programmas) {
            if (Objects.equals(naam, programma.toString())) {
                programmaGevonden = programma;
                break;
            }
        }
        return programmaGevonden;
    }

    /**
     * Nieuwe student aanmaken. Type student wordt bepaald door het gekozen programma
     *
     * @param naam naam van de student
     * @param programmaNaam naam van programma waarop de student zich inschrijft ({@link ReguliereStudent} of {@link
     * Scholer}
     */
    public void newStudent(String naam, String programmaNaam) {
        Student student;
        Programma programma = getProgramma(programmaNaam);
        if (programma instanceof CPP) {
            student = new Scholer(naam, (CPP) programma);
        } else { //TODO: Dat robuuster met een else if
            student = new ReguliereStudent(naam, (Opleiding) programma);
        }
        studenten.add(student);
    }

    /**
     * Zoek een student op basis van de naam. Niets gevonden retourneert een null
     *
     * @param naam naam van de student waarmee gezocht wordt.
     * @return gevonden student
     */
    public Student getStudent(String naam) {
        Student studentGevonden = null;
        for (Student student : studenten) {
            if (naam.equals(student.toString())) {
                studentGevonden = student;
                break;
            }
        }
        return studentGevonden;
    }

    /**
     * verhoog aantal behaalde modules met 1 voor student
     *
     * @param naam student
     */
    public void verhoogModule(String naam) {
        Scholer scholer = (Scholer) getStudent(naam);
        scholer.setModules();
    }


    /**
     * Verhoog aantal studiepunten met aantal voor student
     *
     * @param naam naam van student
     * @param aantal aantal studiepunten dat toegevoegd moet worden.
     */
    public void verhoogStudiepunten(String naam, double aantal) {
        ReguliereStudent regulier = (ReguliereStudent) getStudent(naam);
        regulier.setBehaaldePunten(aantal);
    }

    /**
     * maak een lijst met alle studentgegevens
     *
     * @return lijst van alle studentgegevens
     */
    public ArrayList<String> getStudentenGegevens() {
        ArrayList<String> lijst = new ArrayList<>();
        for (Student student : studenten) {
            lijst.add(getStudentGegevens(student));
        }
        return lijst;
    }

    public String getStudentGegevens(Student student) {
        return student.getInformation();
    }
}
