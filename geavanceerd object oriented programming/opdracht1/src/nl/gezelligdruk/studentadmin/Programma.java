package nl.gezelligdruk.studentadmin;

abstract class Programma {

    private final String naam;

    Programma(String naam) {
        this.naam = naam;
    }

    String getNaam() {
        return naam;
    }

    @Override
    public String toString() {
        return getNaam();
    }
}
