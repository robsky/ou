package nl.gezelligdruk.studentadmin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScholerTest {

    private Scholer student;

    @BeforeEach
    void setUp() throws StudentAdminException {
        CPP cpp = new CPP("Test", 2);
        student = new Scholer("Klaas", cpp);
    }

    @Test
    void setModules() throws StudentAdminException {
        student.verhoogModules();
    }

    @Test
    void getInformation() {
        assertEquals("Klaas, Test, 0 modules, niet geslaagd", student.getInformation());
    }

    @Test
    void indirectTestGeslaagd() throws StudentAdminException {
        student.verhoogModules();
        assertEquals("Klaas, Test, 1 modules, niet geslaagd", student.getInformation());
        student.verhoogModules();
        assertEquals("Klaas, Test, 2 modules, geslaagd", student.getInformation());
    }

    @Test
    void toStringTest() {
        assertEquals("Klaas", student.toString());
    }
}