package nl.gezelligdruk.studentadmin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/*

Helemaal overboord gegaan met tests. Voor de grap eens een 100% code coverage geprobeerd. Tot 99% gekomen. Laatste 1% krijg
ik weg, maar gaat ten koste van leesbaarheid. 99% is ook prima.

 */

class StudentAdminTest {

    private StudentAdmin studentAdmin;

    @BeforeEach
    void setUp() {
        studentAdmin = new StudentAdmin();
    }

    @Test
    void getCpps() {
        ArrayList<String> a = new ArrayList<>();
        a.add("Java");
        a.add("Softwarearchitect");
        a.add("Systeemontwikkelaar");
        assertEquals(a, studentAdmin.getCpps());
    }

    @Test
    void getOpleidingen() {
        ArrayList<String> a = new ArrayList<>();
        a.add("Wiskunde");
        a.add("Informatica");
        assertEquals(a, studentAdmin.getOpleidingen());
    }

    @Test
    void doubleStudent() throws StudentAdminException {
        studentAdmin.newStudent("Klaas", "Java");
        try {
            studentAdmin.newStudent("Klaas", "Java");
            fail("Dubbele student aangemaakt");
        } catch (StudentAdminException e) {
            assertEquals("Student Klaas bestaat al", e.message);
        }
    }

    @Test
    void newStudentTooShortName() {
        try {
            studentAdmin.newStudent("A", "Java");
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Naam moet minimaal uit twee karakters zijn", e.message);
        }
    }

    @Test
    void newStudentTooShortNameTrim() {
        try {
            studentAdmin.newStudent(" A ", "Java");
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Naam moet minimaal uit twee karakters zijn", e.message);
        }
    }

    @Test
    void newStudentInvalidName() {
        try {
            studentAdmin.newStudent("123", "Wiskunde");
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Naam kan alleen letters bevatten", e.message);
        }
    }

    @Test
    void newStudentVeryLongName() {
        String a = "";
        for (int i = 0; i < 201; i++) {
            a = a.concat("a");
        }
        try {
            studentAdmin.newStudent(a, "Java");
        } catch (StudentAdminException e) {
            assertEquals("Wat een onwaarschijnlijk lange naam", e.message);
        }
    }

    @Test
    void newStudentNietBestaandeProgramma() {
        try {
            studentAdmin.newStudent("Jan", "BestaatNiet");
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Programma BestaatNiet bestaat niet", e.message);
        }
    }

    @Test
    void newStudentNullProgramma() {
        try {
            studentAdmin.newStudent("Jan", "");
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Programma  bestaat niet", e.message);
        }

    }

    @Test
    void getStudentNietBestaandeOpleiding() {
        try {
            studentAdmin.newStudent("Jan", "BestaatNiet");
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Programma BestaatNiet bestaat niet", e.message);
        }
    }

    @Test
    void verhoogModule() throws StudentAdminException {
        studentAdmin.newStudent("Jan", "Java");
        String gegevens;
        for (int i = 0; i < 5; i++) {
            studentAdmin.verhoogModule("Jan");
            gegevens = studentAdmin.getStudentGegevens("Jan");
            assertTrue(gegevens.contains(i + 1 + " modules"));
            assertTrue(gegevens.contains("niet geslaagd"));
        }
        studentAdmin.verhoogModule("Jan");
        gegevens = studentAdmin.getStudentGegevens("Jan");
        assertEquals("Jan, Java, 6 modules, geslaagd", gegevens);
    }

    @Test
    void verhoogModuleBovenGeslaagd() throws StudentAdminException {
        studentAdmin.newStudent("Jan", "Java");
        for (int i = 0; i < 6; i++) {
            studentAdmin.verhoogModule("Jan");
        }
        try {
            studentAdmin.verhoogModule("Jan");
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Maximum aantal van 6 modules voor Java is bereikt", e.message);
        }
    }

    @Test
    void verhoogModuleBijStudent() throws StudentAdminException {
        studentAdmin.newStudent("Harrie", "Wiskunde");
        try {
            studentAdmin.verhoogModule("Harrie");
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Student Harrie is geen scholer. Verhogen van modules is niet mogelijk", e.message);
        }
    }

    @Test
    void verhoogPuntenBijScholer() throws StudentAdminException {
        studentAdmin.newStudent("Miep", "Java");
        try {
            studentAdmin.verhoogStudiepunten("Miep", 1);
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Student Miep is geen reguliere student. Verhogen van punten is niet mogelijk", e.message);
        }
    }

    @Test
    void verhoogStudiepunten() throws StudentAdminException {
        studentAdmin.newStudent("Jan", "Wiskunde");
        studentAdmin.verhoogStudiepunten("Jan", 1);
        assertEquals("Jan, Wiskunde, 1.0 behaalde punten, niet geslaagd", studentAdmin.getStudentGegevens("Jan"));
        studentAdmin.verhoogStudiepunten("Jan", 1.1);
        assertEquals("Jan, Wiskunde, 2.1 behaalde punten, niet geslaagd", studentAdmin.getStudentGegevens("Jan"));
        studentAdmin.verhoogStudiepunten("Jan", .1);
        assertEquals("Jan, Wiskunde, 2.2 behaalde punten, niet geslaagd", studentAdmin.getStudentGegevens("Jan"));
    }


    @Test
    void verhoogStudiepunten0() {
        try {
            studentAdmin.newStudent("Jan", "Wiskunde");
            studentAdmin.verhoogStudiepunten("Jan", 0);
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Puntenaantal moet hoger dan 0 zijn.", e.message);
        }
    }

    @Test
    void verhoogStudiepuntenMinus1() throws StudentAdminException {
        studentAdmin.newStudent("Jan", "Wiskunde");
        try {
            studentAdmin.verhoogStudiepunten("Jan", -1);
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Puntenaantal moet hoger dan 0 zijn.", e.message);
        }
    }

    @Test
    void verhoogStudiepuntenMinus1dot1() throws StudentAdminException {
        studentAdmin.newStudent("Jan", "Wiskunde");
        try {
            studentAdmin.verhoogStudiepunten("Jan", -1.1);
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Puntenaantal moet hoger dan 0 zijn.", e.message);
        }
    }

    @Test
    void verhoogStudiepuntenTotGeslaagd() throws StudentAdminException {
        studentAdmin.newStudent("Jan", "Wiskunde");
        String gegevens = studentAdmin.getStudentGegevens("Jan");
        assertEquals("Jan, Wiskunde, 0.0 behaalde punten, niet geslaagd", gegevens);
        studentAdmin.verhoogStudiepunten("Jan", 10);
        gegevens = studentAdmin.getStudentGegevens("Jan");
        assertEquals("Jan, Wiskunde, 10.0 behaalde punten, niet geslaagd", gegevens);
        studentAdmin.verhoogStudiepunten("Jan", 100);
        gegevens = studentAdmin.getStudentGegevens("Jan");
        assertEquals("Jan, Wiskunde, 110.0 behaalde punten, niet geslaagd", gegevens);
        studentAdmin.verhoogStudiepunten("Jan", 50);
        gegevens = studentAdmin.getStudentGegevens("Jan");
        assertEquals("Jan, Wiskunde, 160.0 behaalde punten, geslaagd", gegevens);
    }

    @Test
    void verhoogStudiePuntenBovenMaximum() throws StudentAdminException {
        studentAdmin.newStudent("Jan", "Wiskunde");
        try {
            studentAdmin.verhoogStudiepunten("Jan", 161);
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Puntenaantal wordt groter (161.0) dan maximum aantal punten van opleiding Wiskunde (160.0)", e.message);
        }
    }

    @Test
    void getStudentenGegevensLegeLijst() {
        //assertEquals("[]", studentAdmin.getStudentenGegevens().toString());
        try {
            studentAdmin.getStudentenGegevens();
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Studentenlijst is leeg", e.message);
        }
    }

    @Test
    void getStudentenGegevensLijst() throws StudentAdminException {
        studentAdmin.newStudent("Jan", "Wiskunde");
        assertEquals("[Jan, Wiskunde, 0.0 behaalde punten, niet geslaagd]", studentAdmin.getStudentenGegevens().toString());
        studentAdmin.newStudent("Piet", "Java");
        assertEquals("[Jan, Wiskunde, 0.0 behaalde punten, niet geslaagd, Piet, Java, 0 modules, niet geslaagd]", studentAdmin.getStudentenGegevens().toString());
    }

    @Test
    void  getOnbekendeStudentGegevens() {
        try {
             studentAdmin.getStudentGegevens("jan");
            fail();
        } catch (StudentAdminException e) {
            assertEquals("Student bestaat niet", e.message);
        }
    }
}