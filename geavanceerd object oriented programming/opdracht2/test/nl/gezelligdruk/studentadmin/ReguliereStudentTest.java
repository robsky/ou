package nl.gezelligdruk.studentadmin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReguliereStudentTest {

    private ReguliereStudent student;

    @BeforeEach
    void setUp() throws StudentAdminException {
        Opleiding opleiding = new Opleiding("Test",10);
        student = new ReguliereStudent("Klaas", opleiding);
    }

    @Test
    void setBehaaldePunten() throws StudentAdminException{
        student.verhoogBehaaldePunten(5);
        // returns void, so no easy test. Just shouldn't crash
    }

    @Test
    void getInformation() {
        assertEquals("Klaas, Test, 0.0 behaalde punten, niet geslaagd", student.getInformation());
    }

    @Test
    void  toStringTest(){
        assertEquals("Klaas", student.toString());
    }
}