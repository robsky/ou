package nl.gezelligdruk.studentadminframe;

import nl.gezelligdruk.studentadmin.StudentAdmin;
import nl.gezelligdruk.studentadmin.StudentAdminException;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

@SuppressWarnings({"Convert2Lambda", "FieldCanBeLocal"})
class StudentAdminFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private final StudentAdmin studentAdmin;
    private JPanel jContentPane = null;
    private JScrollPane jScrollPane = null;
    private JTextArea uitvoerGebied = null;
    private JButton toonAlleKnop = null;
    private JTabbedPane mijnTabbladenPanel = null;
    private JPanel voegStudenttoePanel = null;
    private JPanel voegScholertoePanel = null;
    private JPanel studentPanel = null;
    private JPanel alleStudentenPanel = null;
    private JLabel bestaandenaamLabel = null;
    private JTextField bestaandeNaamVeld = null;
    private JLabel infoLabel = null;
    private JTextField studentInfoVeld = null;
    private JLabel nieuwepuntenLabel = null;
    private JTextField puntenVeld = null;
    private JLabel uitlegLabel = null;
    private JButton moduleKnop = null;
    private JLabel opleidingLabel = null;
    private JLabel nstudentLabel = null;
    private JComboBox opleidingComboBox = null;
    private JLabel studentLabel = null;
    private JTextField studentTextField = null;
    private JButton studentButton = null;
    private JComboBox scholingComboBox = null;
    private JLabel scholerLabel = null;
    private JTextField scholerTextField = null;
    private JButton scholerButton = null;

    /**
     * This is the default constructor
     */
    private StudentAdminFrame() {
        super();
        this.studentAdmin = new StudentAdmin();
        initialize();
        events();
    }

    public static void main(String[] args) {
        StudentAdminFrame fr = new StudentAdminFrame();
        fr.setVisible(true);
    }

    /**
     * This method initializes this
     */
    private void initialize() {
        this.setSize(466 * 2, 347);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(getJContentPane());
        this.setTitle("StudentAdministratie");
    }

    /**
     * This method initializes jContentPane
     *
     * @return javax.swing.JPanel
     */
    private JPanel getJContentPane() {
        if (jContentPane == null) {
            infoLabel = new JLabel();
            infoLabel.setBounds(new Rectangle(14, 278, 424 * 2, 33));
            infoLabel.setText("");
            jContentPane = new JPanel();
            jContentPane.setLayout(null);
            jContentPane.add(getMijnTabbladenPanel(), null);
            jContentPane.add(infoLabel, null);
        }
        return jContentPane;
    }

    /**
     * This method initializes jScrollPane
     *
     * @return javax.swing.JScrollPane
     */
    private JScrollPane getJScrollPane() {
        if (jScrollPane == null) {
            jScrollPane = new JScrollPane();
            jScrollPane.setBounds(new Rectangle(6, 65, 382 * 2, 121));
            jScrollPane.setViewportView(getUitvoerGebied());
        }
        return jScrollPane;
    }

    /**
     * This method initializes uitvoerGebied
     *
     * @return javax.swing.JTextArea
     */
    private JTextArea getUitvoerGebied() {
        if (uitvoerGebied == null) {
            uitvoerGebied = new JTextArea();
            uitvoerGebied.setEditable(false);
        }
        return uitvoerGebied;
    }

    /**
     * This method initializes toonAlleKnop
     *
     * @return javax.swing.JButton
     */
    private JButton getToonAlleKnop() {
        if (toonAlleKnop == null) {
            toonAlleKnop = new JButton();
            toonAlleKnop.setText("Toon alle studenten");
            toonAlleKnop.setBounds(new Rectangle(11, 8, 147 * 2, 20));
        }
        return toonAlleKnop;
    }

    /**
     * This method initializes mijnTabbladenPanel
     *
     * @return javax.swing.JTabbedPane
     */
    private JTabbedPane getMijnTabbladenPanel() {
        if (mijnTabbladenPanel == null) {
            mijnTabbladenPanel = new JTabbedPane();
            mijnTabbladenPanel.setBounds(new Rectangle(13, 19, 423 * 2, 224));
            mijnTabbladenPanel.addTab("nieuwe Student", null,
                    getVoegStudenttoePanel(), null);
            mijnTabbladenPanel.addTab("nieuwe Scholer", null,
                    getVoegScholertoePanel(), null);
            mijnTabbladenPanel.addTab("studentinfo", null, getStudentPanel(), null);
            mijnTabbladenPanel.addTab("alle studenten", null,
                    getAlleStudentenPanel(), null);
        }
        return mijnTabbladenPanel;
    }

    /**
     * This method initializes voegStudenttoePanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getVoegStudenttoePanel() {
        if (voegStudenttoePanel == null) {
            studentLabel = new JLabel();
            studentLabel.setBounds(new Rectangle(15, 55, 91 * 2, 24));
            studentLabel.setText("Naam Student");
            nstudentLabel = new JLabel();
            nstudentLabel.setBounds(new Rectangle(16, 16, 146 * 2, 24));
            nstudentLabel.setText("Selecteer een Opleiding");
            voegStudenttoePanel = new JPanel();
            voegStudenttoePanel.setLayout(null);
            voegStudenttoePanel.add(nstudentLabel, null);
            voegStudenttoePanel.add(getOpleidingComboBox(), null);
            voegStudenttoePanel.add(studentLabel, null);
            voegStudenttoePanel.add(getStudentTextField(), null);
            voegStudenttoePanel.add(getStudentButton(), null);
        }
        return voegStudenttoePanel;
    }

    private JPanel getVoegScholertoePanel() {
        if (voegScholertoePanel == null) {
            scholerLabel = new JLabel();
            scholerLabel.setBounds(new Rectangle(16, 54, 116 * 2, 25));
            scholerLabel.setText("Naam Scholer");
            opleidingLabel = new JLabel();
            opleidingLabel.setBounds(new Rectangle(16, 16, 173 * 2, 25));
            opleidingLabel.setText("Selecteer een CPP-Opleiding");
            voegScholertoePanel = new JPanel();
            voegScholertoePanel.setLayout(null);
            voegScholertoePanel.add(opleidingLabel, null);
            voegScholertoePanel.add(getScholingComboBox(), null);
            voegScholertoePanel.add(scholerLabel, null);
            voegScholertoePanel.add(getScholerTextField(), null);
            voegScholertoePanel.add(getScholerButton(), null);

        }
        return voegScholertoePanel;
    }

    /**
     * This method initializes studentPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getStudentPanel() {
        if (studentPanel == null) {
            uitlegLabel = new JLabel();
            uitlegLabel.setBounds(new Rectangle(16, 8, 334 * 2, 19));
            uitlegLabel.setText("Geef enter om invoer te bevestigen");
            nieuwepuntenLabel = new JLabel();
            nieuwepuntenLabel.setBounds(new Rectangle(14, 63, 256 * 2, 20));
            nieuwepuntenLabel.setText("Punten behaald (alleen reguliere Opleiding) ");
            bestaandenaamLabel = new JLabel();
            bestaandenaamLabel.setBounds(new Rectangle(14, 35, 86 * 2, 20));
            bestaandenaamLabel.setText("Studentnaam");
            studentPanel = new JPanel();
            studentPanel.setLayout(null);
            studentPanel.add(bestaandenaamLabel, null);
            studentPanel.add(getBestaandeNaamVeld(), null);
            studentPanel.add(getStudentInfoVeld(), null);
            studentPanel.add(nieuwepuntenLabel, null);
            studentPanel.add(getPuntenVeld(), null);
            studentPanel.add(uitlegLabel, null);
            studentPanel.add(getModuleKnop(), null);
        }
        return studentPanel;
    }

    /**
     * This method initializes alleStudentenPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getAlleStudentenPanel() {
        if (alleStudentenPanel == null) {
            alleStudentenPanel = new JPanel();
            alleStudentenPanel.setLayout(null);
            alleStudentenPanel.add(getToonAlleKnop(), null);
            alleStudentenPanel.add(getJScrollPane(), null);
        }
        infoLabel.setText("");
        return alleStudentenPanel;
    }

    /**
     * This method initializes bestaandeNaamVeld
     *
     * @return javax.swing.JTextField
     */
    private JTextField getBestaandeNaamVeld() {
        if (bestaandeNaamVeld == null) {
            bestaandeNaamVeld = new JTextField();
            bestaandeNaamVeld.setBounds(new Rectangle(114, 36, 151 * 2, 20));
        }
        return bestaandeNaamVeld;
    }

    /**
     * This method initializes studentInfoVeld
     *
     * @return javax.swing.JTextField
     */
    private JTextField getStudentInfoVeld() {
        if (studentInfoVeld == null) {
            studentInfoVeld = new JTextField();
            studentInfoVeld.setBounds(new Rectangle(10, 152, 392 * 2, 27));
            studentInfoVeld.setEditable(false);
        }
        return studentInfoVeld;
    }

    /**
     * This method initializes puntenVeld
     *
     * @return javax.swing.JTextField
     */
    private JTextField getPuntenVeld() {
        if (puntenVeld == null) {
            puntenVeld = new JTextField();
            puntenVeld.setBounds(new Rectangle(284, 63, 47 * 2, 20));
        }
        return puntenVeld;
    }

    /**
     * This method initializes moduleKnop
     *
     * @return javax.swing.JButton
     */
    private JButton getModuleKnop() {
        if (moduleKnop == null) {
            moduleKnop = new JButton();
            moduleKnop.setBounds(new Rectangle(14, 91, 328 * 2, 21));
            moduleKnop.setText("Module behaald (alleen CPP)");
        }
        return moduleKnop;
    }

    /**
     * This method initializes opleidingComboBox
     *
     * @return javax.swing.JComboBox
     */
    private JComboBox getOpleidingComboBox() {
        if (opleidingComboBox == null) {
            opleidingComboBox = new JComboBox();
            opleidingComboBox.setBounds(new Rectangle(195, 16, 200 * 2, 24));

            for (String opleiding : studentAdmin.getOpleidingen()) {
                opleidingComboBox.addItem(opleiding);
            }
        }
        return opleidingComboBox;
    }

    /**
     * This method initializes studentTextField
     *
     * @return javax.swing.JTextField
     */
    private JTextField getStudentTextField() {
        if (studentTextField == null) {
            studentTextField = new JTextField();
            studentTextField.setBounds(new Rectangle(195, 55, 197 * 2, 24));
        }
        return studentTextField;
    }

    /**
     * This method initializes studentButton
     *
     * @return javax.swing.JButton
     */
    private JButton getStudentButton() {
        if (studentButton == null) {
            studentButton = new JButton();
            studentButton.setBounds(new Rectangle(15, 106, 153 * 2, 24));
            studentButton.setText("Voeg Student toe");
        }
        return studentButton;
    }

    /**
     * This method initializes scholingComboBox
     *
     * @return javax.swing.JComboBox
     */
    private JComboBox getScholingComboBox() {
        if (scholingComboBox == null) {
            scholingComboBox = new JComboBox();
            scholingComboBox.setBounds(new Rectangle(210, 16, 167 * 2, 25));

            for (String cpp : studentAdmin.getCpps()) {
                scholingComboBox.addItem(cpp);
            }
        }
        return scholingComboBox;
    }

    /**
     * This method initializes scholerTextField
     *
     * @return javax.swing.JTextField
     */
    private JTextField getScholerTextField() {
        if (scholerTextField == null) {
            scholerTextField = new JTextField();
            scholerTextField.setBounds(new Rectangle(210, 54, 166 * 2, 25));
        }
        return scholerTextField;
    }

    /**
     * This method initializes scholerButton
     *
     * @return javax.swing.JButton
     */
    private JButton getScholerButton() {
        if (scholerButton == null) {
            scholerButton = new JButton();
            scholerButton.setBounds(new Rectangle(14, 103, 163 * 2, 25));
            scholerButton.setText("Voeg Scholer toe");
        }
        return scholerButton;
    }

    private void events() {

/*
Infolabel leeghalen bij wisselen tab

 */
        mijnTabbladenPanel.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                infoLabel.setText("");
            }
        });

        /*
          Create new student

         */
        studentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                infoLabel.setText("");
                String student = studentTextField.getText();
                studentTextField.setText("");
                String programma;
                try {
                    programma = opleidingComboBox.getSelectedItem().toString();
                    studentAdmin.newStudent(student, programma);
                    infoLabel.setText("Student aangemaakt");
                } catch (StudentAdminException err) {
                    infoLabel.setText(err.message);
                } catch (NullPointerException err) {
                    // NPE's hoeven we niet af te vangen, maar om deze (en de volgende) bleef de code inspection zeuren.
                    infoLabel.setText("Er is geen keuze voor opleiding gemaakt");
                }
            }
        });

        /*
          Create new scholer

         */
        scholerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                infoLabel.setText("");
                String scholer = scholerTextField.getText();
                scholerTextField.setText("");
                try {
                    studentAdmin.newStudent(scholer, scholingComboBox.getSelectedItem().toString());
                    infoLabel.setText("Scholer aangemaakt");
                } catch (StudentAdminException err) {
                    infoLabel.setText(err.message);
                } catch (NullPointerException err) {
                    infoLabel.setText("Er is geen keuze voor CPP gemaakt");
                }
            }
        });

        /*
          Get all students
         */
        toonAlleKnop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                infoLabel.setText("");
                uitvoerGebied.setText("");
                try {
                    ArrayList<String> students = studentAdmin.getStudentenGegevens();
                    for (String student : students) {
                        uitvoerGebied.append(student + "\n");
                    }
                } catch (StudentAdminException ex) {
                    infoLabel.setText(ex.message);
                }
            }
        });

        /*
          add module to scholer

         */
        moduleKnop.addActionListener(new ActionListener() {
            @Override

            public void actionPerformed(ActionEvent e) {
                try {
                    infoLabel.setText("");
                    studentAdmin.verhoogModule(bestaandeNaamVeld.getText());
                    infoLabel.setText("Module voor scholer " + bestaandeNaamVeld.getText() + " verhoogd");
                    studentInfoVeld.setText(studentAdmin.getStudentGegevens(bestaandeNaamVeld.getText()));
                } catch (StudentAdminException ex) {
                    infoLabel.setText(ex.message);
                }
            }
        });

        /*
          add punten to student

         */
        puntenVeld.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                infoLabel.setText("");
                try {
                    studentAdmin.verhoogStudiepunten(bestaandeNaamVeld.getText(), Double.parseDouble(puntenVeld.getText()));
                    infoLabel.setText("Aantal punten van student " + bestaandeNaamVeld.getText() + " verhoogd");
                    studentInfoVeld.setText(studentAdmin.getStudentGegevens(bestaandeNaamVeld.getText()));
                } catch (StudentAdminException err) {
                    infoLabel.setText(err.message);
                } catch (NumberFormatException err) {
                    infoLabel.setText("Alleen nummers toegestaan");
                }
            }
        });

        /*
          Print studentinformation
         */
        bestaandeNaamVeld.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                infoLabel.setText("");
                try {
                    studentInfoVeld.setText(studentAdmin.getStudentGegevens(bestaandeNaamVeld.getText()));
                } catch (StudentAdminException err) {
                    infoLabel.setText(err.message);
                }
            }
        });

    }
}
