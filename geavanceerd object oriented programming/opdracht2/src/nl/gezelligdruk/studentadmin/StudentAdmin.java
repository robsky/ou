package nl.gezelligdruk.studentadmin;

import java.util.ArrayList;
import java.util.List;

/**
 * Studentadmin is de toegangsklasse voor het domein model van de applicatie.
 */
public class StudentAdmin {
    private final List<Student> studenten;
    private final List<Programma> programmas;

    /**
     * creer nieuwe studentenadministratie. Iniatiliseer opleidingen en cpp's
     */
    public StudentAdmin() {
        this.studenten = new ArrayList<>();
        this.programmas = new ArrayList<>();

        programmas.add(new Opleiding("Wiskunde", 160));
        programmas.add(new Opleiding("Informatica", 200));
        programmas.add(new CPP("Java", 6));
        programmas.add(new CPP("Softwarearchitect", 4));
        programmas.add(new CPP("Systeemontwikkelaar", 3));
    }

    /**
     * @return lijst van ccp's
     */
    public ArrayList<String> getCpps() {
        ArrayList<String> cpps = new ArrayList<>();
        for (Programma programma : programmas) {
            if (programma instanceof CPP) {
                cpps.add(programma.toString());
            }
        }
        return cpps;
    }

    /**
     * lijst van opleidingen
     *
     * @return lijst van opleidingen
     */
    public ArrayList<String> getOpleidingen() {
        ArrayList<String> opleidingen = new ArrayList<>();
        for (Programma programma : programmas) {
            if (programma instanceof Opleiding) {
                opleidingen.add(programma.toString());
            }
        }
        return opleidingen;
    }

    /**
     * Zoek een programma op basis van de naam.
     *
     * @param naam naam van het programma
     * @return gevonden programma ({@link CPP} of {@link Opleiding})
     * @throws StudentAdminException als programma niet bestaat
     */
    private Programma getProgramma(String naam) throws StudentAdminException {
        Programma programmaGevonden = null;
        for (Programma programma : programmas) {
            if (naam.equals(programma.toString())) {
                programmaGevonden = programma;
                break;
            }
        }
        if (programmaGevonden == null) {
            throw new StudentAdminException("Programma " + naam + " bestaat niet");
        }
        return programmaGevonden;
    }

    /**
     * Nieuwe student aanmaken. Type student wordt bepaald door het gekozen programma
     *
     * @param naam naam van de student
     * @param programmaNaam naam van programma waarop de student zich inschrijft ({@link ReguliereStudent} of {@link
     * Scholer}
     * @throws StudentAdminException als student al bestaat
     */
    public void newStudent(String naam, String programmaNaam) throws StudentAdminException {
        naam = naam.trim();
        if (getStudent(naam) != null) {
            throw new StudentAdminException("Student " + naam + " bestaat al");
        }
        Student student;
        Programma programma = getProgramma(programmaNaam);
        if (programma instanceof CPP) {
            student = new Scholer(naam, (CPP) programma);
        } else {
            student = new ReguliereStudent(naam, (Opleiding) programma);
        }
        studenten.add(student);
    }

    /**
     * Zoek een student op basis van de naam. Niets gevonden retourneert een null
     *
     * @param naam naam van de student waarmee gezocht wordt.
     * @return gevonden student
     */
    private Student getStudent(String naam) {
        Student studentGevonden = null;
        for (Student student : studenten) {
            if (naam.equals(student.toString())) {
                studentGevonden = student;
                break;
            }
        }
        return studentGevonden;
    }

    /**
     * verhoog aantal behaalde modules met 1 voor student
     *
     * @param naam student
     * @throws StudentAdminException als verkeerd type student gebruikt wordt
     */
    public void verhoogModule(String naam) throws StudentAdminException {
        Scholer scholer;
        if (getStudent(naam) instanceof Scholer) {
            scholer = (Scholer) getStudent(naam);
        } else {
            throw new StudentAdminException("Student " + naam + " is geen scholer. Verhogen van modules is niet mogelijk");
        }
        scholer.verhoogModules();
    }


    /**
     * Verhoog aantal studiepunten met aantal voor student
     *
     * @param naam naam van student
     * @param aantal aantal studiepunten dat toegevoegd moet worden.
     * @throws StudentAdminException als verkeerd type student gebruikt wordt.
     */
    public void verhoogStudiepunten(String naam, double aantal) throws StudentAdminException {
        ReguliereStudent regulier;
        if (getStudent(naam) instanceof ReguliereStudent) {
            regulier = (ReguliereStudent) getStudent(naam);
        } else {
            throw new StudentAdminException("Student " + naam + " is geen reguliere student. Verhogen van punten is niet mogelijk");
        }
        regulier.verhoogBehaaldePunten(aantal);
    }

    /**
     * maak een lijst met alle studentgegevens
     *
     * @return lijst van alle studentgegevens
     * @throws StudentAdminException Als student niet bestaat
     */
    public ArrayList<String> getStudentenGegevens() throws StudentAdminException {
        ArrayList<String> lijst = new ArrayList<>();
        if (studenten.size() == 0) {
            throw new StudentAdminException("Studentenlijst is leeg");
        }
        for (Student student : studenten) {
            lijst.add(student.getInformation());
        }
        return lijst;
    }

    /**
     * Haal studentgegevens op
     *
     * @param naam String object
     * @return gevonden studentgegevens
     * @throws StudentAdminException Als de student niet bestaat
     */
    public String getStudentGegevens(String naam) throws StudentAdminException {
        //Student student;
        for (Student student : studenten) {
            if (student.getNaam().equals(naam)) {
                return student.getInformation();
            }
        }
        throw new StudentAdminException("Student bestaat niet");
    }
}
