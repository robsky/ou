package nl.gezelligdruk.studentadmin;

/**
 * Retuliere student is een extension van student. Bestaat uit een naam, en een {@link Opleiding}. Een
 * student is geslaagd voor een opleiding als behaaldePunten gelijk is aan het aantal studiepunten van de
 * opleiding
 *
 */
class ReguliereStudent extends Student {

    private final Opleiding opleiding;
    private double behaaldePunten = 0.0;

    /**
     * Creer nieuwe reguliere student
     * @param naam naam van de student
     * @param opleiding opleiding waarop de student ingeschreven is
     * @throws StudentAdminException als constructor in {@link Student} een fout geeft.
     */
    ReguliereStudent(String naam, Opleiding opleiding) throws StudentAdminException {
        super(naam);
        this.opleiding = opleiding;
    }

    /**
     * Test of student geslaagd is voor opleiding
     *
     * @return True als geslaagd
     */
    private Boolean isGeslaagd() {
        boolean geslaagd = false;
        if (behaaldePunten == opleiding.getStudiePunten()) {
            geslaagd = true;
        }
        return geslaagd;
    }

    private double getBehaaldePunten() {
        return behaaldePunten;
    }

    /**
     * Verhoog aantal behaalde punten
     *
     * @param behaaldePunten aantal waarmee het aantal behaalde punten verhoogt wordt
     * @throws StudentAdminException als punten <= 0 is of als totaal boven aantal van opleiding komt
     */
    public void verhoogBehaaldePunten(double behaaldePunten) throws StudentAdminException {
        double totaal = this.behaaldePunten + behaaldePunten;
        if (behaaldePunten <= 0) {
            throw new StudentAdminException("Puntenaantal moet hoger dan 0 zijn.");
        } else if (totaal > opleiding.getStudiePunten()) {
            throw new StudentAdminException("Puntenaantal wordt groter (" + totaal + ") dan maximum aantal punten van opleiding " + opleiding.getNaam() + " (" + opleiding.getStudiePunten() + ")");
        }
        this.behaaldePunten += behaaldePunten;
    }

    protected String getInformation() {
        return getNaam() + ", " + opleiding.getNaam() + ", " + getBehaaldePunten() + " behaalde punten, " + (isGeslaagd() ? "geslaagd" : "niet geslaagd");
    }
}
