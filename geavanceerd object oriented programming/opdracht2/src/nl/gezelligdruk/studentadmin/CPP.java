package nl.gezelligdruk.studentadmin;

/**
 * CPP is een extension van programma. Heeft een naam en een aantal modules
 *
 */
class CPP extends Programma {

    private final Integer aantalmodules;

    /**
     * Creer cpp programma.
     * @param naam naam van CPP
     * @param aantalModules aantal modules van het CPP
     */
    CPP(String naam, int aantalModules) {
        super(naam);
        this.aantalmodules = aantalModules;
    }

    Integer getAantalmodules() {
        return aantalmodules;
    }
}