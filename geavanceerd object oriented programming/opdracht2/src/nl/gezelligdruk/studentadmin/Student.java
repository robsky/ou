package nl.gezelligdruk.studentadmin;

/**
 * Abstracte class Student. Bestaat uit een naam, met een aantal voorwaarden.
 *
 */
abstract class Student {

    private final String naam;

    /**
     * Creer student
     * @param naam naam van de student
     * @throws StudentAdminException als niet aan de voorwaarden voor een correcte naam voldaan wordt
     */
    Student(String naam) throws StudentAdminException {
        if (naam.length() < 2) {
            throw new StudentAdminException("Naam moet minimaal uit twee karakters zijn");
        }
        if (naam.length() > 200) {
            throw new StudentAdminException("Wat een onwaarschijnlijk lange naam");
        }
        if (!naam.matches("[A-Za-z]*")) {
            throw new StudentAdminException("Naam kan alleen letters bevatten");
        }
        this.naam = naam;
    }

    String getNaam() {
        return naam;
    }

    protected abstract String getInformation();

    @Override
    public String toString() {
        return naam;
    }
}
