package nl.gezelligdruk.studentadmin;

/**
 * Opleiding is een extension van programma. Bestaat uit een naam en een aantal studiepunten
 *
 */
class Opleiding extends Programma {

    private final double studiePunten;

    /**
     * Creer Opleiding
     * @param naam naam van de opleiding
     * @param studiePunten aantal studiepunten waaruit opleiding bestaat
     */
    Opleiding(String naam, double studiePunten) {
        super(naam);
        this.studiePunten = studiePunten;
    }

    double getStudiePunten() {
        return studiePunten;
    }
}
