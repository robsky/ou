package nl.gezelligdruk.studentadmin;

/**
 * Abstracte class programma. Bestaat uit een naam en een overridden toString methode
 *
 */
abstract class Programma {

    private final String naam;

    /**
     * Constructor voor programma
     *
     * @param naam naam van het programma
     */
    Programma(String naam) {
        this.naam = naam;
    }

    String getNaam() {
        return naam;
    }

    @Override
    public String toString() {
        return getNaam();
    }
}