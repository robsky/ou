package nl.gezelligdruk.studentadmin;

/**
 * Exception specifiek voor de StudentAdmin applicatie
 */
public class StudentAdminException extends Exception {

    public final String message;

    public StudentAdminException(String message) {
        super(message);
        this.message = message;
    }
}
