package nl.gezelligdruk.studentadmin;

/**
 * Een Scholer is een extension van student. Bestaat uit een naam en een ingeschreven CPP. Scholer
 * is geslaagd als het aantal behaalde modules gelijk is aan het aantal modules van de CPP.
 *
 */
class Scholer extends Student {

    //private final CPP cpp;
    private int modules = 0;
    private final CPP cpp;

    /**
     * Constructor voor een scoler
     *
     * @param naam naam van de scholer
     * @param cpp opleiding waarop de scholer ingeschreven wordt
     * @throws StudentAdminException eigen exception voor studentadmin
     */
    Scholer(String naam, CPP cpp) throws StudentAdminException {
        super(naam);
        this.cpp = cpp;
    }

    private int getModules() {
        return modules;
    }

    /**
     * verhoog aantal behaalde modules met 1
     *
     * @throws StudentAdminException als student al geslaagd is
     */
    void verhoogModules() throws StudentAdminException {
        if (isGeslaagd()) {
            throw new StudentAdminException("Maximum aantal van " + cpp.getAantalmodules() + " modules voor " + cpp.getNaam() + " is bereikt");
        }
        modules++;
    }

    /**
     * Retourneer alle informatie van een student
     *
     * @return string met alle informatie van een student
     */
    protected String getInformation() {
        return getNaam() + ", " + cpp.getNaam() + ", " + getModules() + " modules, " + (isGeslaagd() ? "geslaagd" : "niet geslaagd");
    }

    /**
     * Is een student geslaagd of niet, afhankelijk of het aantal behaalde modules groter of gelijk is aan het aantal
     * benodigde modules van de ccp van de student.
     *
     * @return true als student geslaagd is
     */
    private Boolean isGeslaagd() {
        boolean geslaagd = false;
        if (modules == cpp.getAantalmodules()) {
            geslaagd = true;
        }
        return geslaagd;
    }
}
