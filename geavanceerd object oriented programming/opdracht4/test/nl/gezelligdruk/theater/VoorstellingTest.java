package nl.gezelligdruk.theater;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VoorstellingTest {

    Voorstelling voorstelling;
    Plaats plaats;
    Klant klant;

    @BeforeEach
    void setUp(){
        GregorianCalendar calendar = new GregorianCalendar();
        voorstelling = new Voorstelling("test", calendar);
        plaats = voorstelling.getPlaats(1, 1);
        klant = new Klant(123, "Rob", "1");

    }
//
//    @Test
//    void reserveerPlaats() {
//        int voor = voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD);
//        voorstelling.reserveerPlaats(plaats);
//        int na = voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD);
//        assertEquals(voor+1, na);
//
//        Plaats plaats1 = new Plaats(-1, -1);
//        voorstelling.reserveerPlaats(plaats1);
//        int na2 = voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD);
//        assertEquals(na,na2);
//
//        Plaats plaats2 = new Plaats(0, 0);
//        voorstelling.reserveerPlaats(plaats2);
//        int na3 = voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD);
//        assertEquals(na,na2);
//
//        Plaats plaats3 = new Plaats(9999, 99999);
//        voorstelling.reserveerPlaats(plaats3);
//        int na4 = voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD);
//        assertEquals(na3, na4);
//    }
//
//    @Test
//    void resetReservering() {
//        int voor = voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD);
//        voorstelling.reserveerPlaats(plaats);
//        assertEquals(voor+1, voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD));
//        voorstelling.resetReservering();
//        assertEquals(voor, voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD));
//
//    }
//
//    @Test
//    void bevestingReservering() {
//        int voor = voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD);
//        assertEquals(0, voorstelling.getAantalPlaatsen(plaatsStatus.BEZET));
//        voorstelling.reserveerPlaats(plaats);
//        voorstelling.bevestingReservering(klant);
//        assertEquals(1, voorstelling.getAantalPlaatsen(plaatsStatus.BEZET));
//        assertEquals(voor, voorstelling.getAantalPlaatsen(plaatsStatus.GERESERVEERD));
//    }

    @Test
    void getPlaats() {
        assertEquals(plaats, voorstelling.getPlaats(1, 1 ));
    }
}