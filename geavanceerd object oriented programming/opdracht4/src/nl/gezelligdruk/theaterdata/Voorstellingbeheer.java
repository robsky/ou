package nl.gezelligdruk.theaterdata;

import nl.gezelligdruk.theater.TheaterException;
import nl.gezelligdruk.theater.Voorstelling;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.logging.Logger;


/**
 * Klasse die met voorstellingen beheert. Op elke datum is er maar een voorstelling. Deze klasse moet gewijzigd worden
 * zodat ArrayList vervangen wordt door gebruik database.
 */
public class Voorstellingbeheer {
    private static final Logger LOGGER = Logger.getLogger(Voorstellingbeheer.class.getName());
    // tijdelijk; in databaseversie niet meer nodig
    private static ArrayList<Voorstelling> voorstellingen = new ArrayList<Voorstelling>();

    /**
     * Vult voorstellingbeheer met een aantal voorstellingen.
     */
    public static void init() {
//        voorstellingen.add(new Voorstelling("Carmen", new GregorianCalendar(2019,
//                Calendar.SEPTEMBER, 21)));
//        voorstellingen.add(new Voorstelling("Ramses", new GregorianCalendar(2019,
//                Calendar.SEPTEMBER, 24)));
    }

    /**
     * Levert alle data op waarop voorstellingen zijn (voor zover die data in de toekomst liggen).
     *
     * @return lijst met data van voorstellingen
     */
    public static ArrayList<GregorianCalendar> geefVoorstellingsData() throws TheaterException, SQLException {
        GregorianCalendar nu = new GregorianCalendar();
        ArrayList<GregorianCalendar> data = new ArrayList<>();
        ResultSet res = Connectiebeheer.queryDB("SELECT datum FROM voorstelling");
        try {
            while (res.next()) {
                GregorianCalendar datum = new GregorianCalendar();
                datum.setTimeInMillis(res.getDate("datum").getTime());
                LOGGER.info("\n1 " + nu.toString() + "\n2 " + datum.toString());
                if (nu.before(data)) {
                    data.add(datum);
                } else {
                    LOGGER.info("nix nakkes nada");
                }
            }
        } catch (SQLException e) {
        }
//        for (Voorstelling voorstelling : voorstellingen) {
//            GregorianCalendar datum = voorstelling.getDatum();
//            if (datum.after(nu)) {
//                data.add(datum);
//            }
//        }

        if (data.size() > 0) {
            return data;
        } else {
            throw new TheaterException("Geen toekomstige voorstellingen gevonden");
        }
    }

    /**
     * Zoekt een voorstelling op de gegeven datum.
     *
     * @param datum
     * @return een voorstelling op de gegeven datum of null wanneer die voorstelling er niet is.
     */
    public static Voorstelling geefVoorstelling(GregorianCalendar datum) {
        for (Voorstelling voorstelling : voorstellingen) {
            if (voorstelling.getDatum().equals(datum)) {
                return voorstelling;
            }
        }
        return null;
    }
}
