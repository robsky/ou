package nl.gezelligdruk.theaterdata;

import nl.gezelligdruk.theater.TheaterException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Beheert de connectie met de database. Bevat methoden voor openen en sluiten van connectie met database, en voor
 * opvragen van de connectie.
 *
 * @author Open Universiteit
 */
public class Connectiebeheer {

    private static final Logger LOGGER = Logger.getLogger(Connectiebeheer.class.getName());

    public static Connection conn = null;

    /**
     * Snelle simpele manier om verbinding met database te controleren
     *
     * @param args mag leeg blijven
     * @throws TheaterException Als er wat mis gaat hoor je dat hier.
     */
    public static void main(String[] args) throws TheaterException {
        try {
            openDB();
        } catch (TheaterException e) {
            throw new TheaterException(e.getMessage());
        }
        try {
            closeDB();
        } catch (TheaterException e) {
            throw new TheaterException(e.getMessage());
        }
    }

    /**
     * Maakt een connectie met de database en initialiseert Klantbeheer en VoostellingBeheer.
     *
     * @throws TheaterException als de initialisatie mislukt.
     */
    public static void openDB() throws TheaterException {
        try {
            conn = DriverManager.getConnection(DBConst.URL, DBConst.GEBRUIKERSNAAM, DBConst.WACHTWOORD);
            LOGGER.info("Connectie met database geslaagd");
        } catch (SQLException ex) {
            String msg = "Probleem met verbinden met database " + ex.getMessage();
            LOGGER.warning(msg);
            throw new TheaterException(msg);
        }
    }

    /**
     * Sluit de connectie met de database
     */
    public static void closeDB() throws TheaterException {
        try {
            conn.close();
            LOGGER.info("Afsluiten database geslaagd");
        } catch (SQLException ex) {
            throw new TheaterException("Probleem bij afsluiten database connectie " + ex.getMessage());
        }
    }

    public static ResultSet queryDB(String query) throws TheaterException {
        LOGGER.info(query);
        try {
            return conn.prepareStatement(query).executeQuery();
        } catch (SQLException e) {
            throw new TheaterException("Fout in query " + query + " " + e.getMessage());
        }
    }
}
