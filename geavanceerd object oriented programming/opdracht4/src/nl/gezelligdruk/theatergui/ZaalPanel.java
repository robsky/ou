package nl.gezelligdruk.theatergui;

import nl.gezelligdruk.theater.Plaats;
import nl.gezelligdruk.theater.Theater;
import nl.gezelligdruk.theater.Voorstelling;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;

/**
 * Deze klasse representeert de zaalbezetting van de geselecteerde voorstelling.
 * <ul>
 * <li>Het zaalPanel vraagt bij de voorstelling ��n voor ��n de plaatsen op,</li>
 * <li>Het zaalPanel maakt voor elke plaats een plaatsPanel</li>
 * <li>Het zaalPanel voegt de muisluisteraar aan het plaatsPanel toe.</li>
 * </ul>
 */
public class ZaalPanel extends JPanel {

    public static final int HGAP = 4;
    public static final int VGAP = 10;
    public static final int AFMETING = 350;
    private static final long serialVersionUID = 1L;

    public ZaalPanel(Voorstelling voorstelling, MouseListener muisLuisteraar) {
        setSize(AFMETING, AFMETING);
        setLayout(new GridLayout(Theater.AANTALRIJEN + 1, Theater.AANTALPERRIJ + 1,
                HGAP, VGAP));
        // Eerst een rij met stoelnummers
        this.add(new JLabel(""));
        for (int stoelnr = 1; stoelnr <= Theater.AANTALPERRIJ; stoelnr++) {
            this.add(new JLabel("" + stoelnr, SwingConstants.CENTER));
        }
        // Elke kolom begint ook met een rijnummer
        for (int rijnr = 1; rijnr <= Theater.AANTALRIJEN; rijnr++) {
            this.add(new JLabel(rijnr + ""));
            for (int stoelnr = 1; stoelnr <= Theater.AANTALPERRIJ; stoelnr++) {
                Plaats plaats = voorstelling.getPlaats(rijnr, stoelnr);
                PlaatsPanel plaatsPanel = new PlaatsPanel(plaats, muisLuisteraar);
                this.add(plaatsPanel);
            }
        }
    }

}