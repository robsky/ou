package nl.gezelligdruk.theater;

import nl.gezelligdruk.theaterdata.Connectiebeheer;
import nl.gezelligdruk.theaterdata.Klantbeheer;
import nl.gezelligdruk.theaterdata.Voorstellingbeheer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Representeert (de kassa van) een theater.
 *
 * @author Open Universiteit
 */
public class Theater {
    public static final int AANTALPERRIJ = 10;
    public static final int AANTALRIJEN = 15;
    private String naam = null;
    private Voorstelling huidigeVoorstelling = null;

    // Logging format instellen
    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT [%4$s] %2$s: %5$s%n");
    }

    String name;
    private static final Logger LOGGER = Logger.getLogger(Theater.class.getName());

    /**
     * Creeert een theater.
     *
     * @param naam theaternaam
     */
    public Theater(String naam) throws TheaterException {
        this.naam = naam;
        Connectiebeheer.openDB();
        Klantbeheer.init();
        Voorstellingbeheer.init();
    }

    public void sluit() throws TheaterException {
        Connectiebeheer.closeDB();
        LOGGER.log(Level.INFO, "Database verbinding afgesloten");
    }

    /**
     * Geeft de naam van het theater.
     *
     * @return naam van het theater
     */
    public String getNaam() {
        return naam;
    }

    /**
     * Geeft een lijst van data waarop voorstellingen zijn gepland.
     *
     * @return lijst met data.
     */
    public ArrayList<GregorianCalendar> geefVoorstellingsData() throws TheaterException, SQLException {
        return Voorstellingbeheer.geefVoorstellingsData();
    }

    /**
     * Wisselt de huidige voorstelling naar voorstelling met gegeven datum.
     *
     * @param datum datum van gevraagde voorstelling
     */
    public void wisselVoorstelling(GregorianCalendar datum) {
        huidigeVoorstelling = Voorstellingbeheer.geefVoorstelling(datum);
    }

    /**
     * Geeft de huidige voorstelling.
     *
     * @return de huidige voorstelling
     */
    public Voorstelling getHuidigeVoorstelling() {
        return huidigeVoorstelling;
    }

    /**
     * Plaatst een klant op alle gereserveerde stoelen van de huidige uitvoering. Wanneer een klant nog niet bestaat,
     * dan wordt deze eerst gecreeerd.
     *
     * @param naam naam van klant
     * @param telefoon telefoonnummer van klant
     */
    public void plaatsKlant(String naam, String telefoon) {
        Klant klant = Klantbeheer.geefKlant(naam, telefoon);
        huidigeVoorstelling.plaatsKlant(klant);
    }

    /**
     * Verandert de reserveringsstatus (VRIJ<=>GERESERVEERD) van een plaats in de huidige voorstelling.
     *
     * @param rijnummer rijnummer van plaats
     * @param stoelnummer stoelnummer van plaats
     * @return true als verandering is gelukt, anders false
     */
    public boolean veranderReservering(int rijnummer, int stoelnummer) {
        return huidigeVoorstelling.veranderReservering(rijnummer, stoelnummer);

    }

    /**
     * Geeft informatie over een plaats in de huidige voorstelling.
     *
     * @param rijnummer rijnummer van plaats
     * @param stoelnummer stoelnummer van plaats
     * @return informatie over plaats
     */
    public String geefPlaatsInfo(int rijnummer, int stoelnummer) {
        Plaats plaats = huidigeVoorstelling.getPlaats(rijnummer, stoelnummer);
        return plaats.toString();
    }
}
