package nl.gezelligdruk.theater;

public class TheaterException extends Throwable {
    public TheaterException(String s) {
        super(s);
    }

    public TheaterException() {
        super();
    }
}
