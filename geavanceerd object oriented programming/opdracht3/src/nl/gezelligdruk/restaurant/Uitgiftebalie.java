package nl.gezelligdruk.restaurant;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * The type Uitgiftebalie. Hier worden door {@link Kok} maaltijden geplaatst en door {@link Ober} {@link Maaltijd} opgehaald.
 * Geimplementeerd als een First-in-first-out queue. De kok plaatst achteraan de balie. De Kok haalt de oudste maaltijden van de balie.
 */
class Uitgiftebalie {

    private static final Logger LOGGER = Logger.getLogger(Uitgiftebalie.class.getName());
    private final List<Maaltijd> maaltijdenOpBalie = new ArrayList<>();

    /**
     * Plaats maaltijd achteraan de op de uitgiftebalie. Synchronised methode, zodat de verschillende koks
     * om de beurt een maaltijd op de balie kunnen plaatsen
     *
     * @param maaltijd de maaltijd die op de balie geplaatst wordt.
     */
    synchronized void plaatsMaaltijd(Maaltijd maaltijd) {
        maaltijdenOpBalie.add(maaltijd);
        LOGGER.info(maaltijd.toString() + " op balie geplaatst. Er staan nu " + maaltijdenOpBalie.size() + " maaltijden");
    }

    /**
     * Pak de volgende maaltijd van de balie. Synchronised methode, zodat de verschillende obers
     * om de beurt een maaltijd kunnen pakken
     *
     * @return de maaltijd opgepakte maaltijd
     */
    synchronized Maaltijd pakMaaltijd() {
        Maaltijd maaltijd = null;
        if (!maaltijdenOpBalie.isEmpty()) {
            maaltijd = maaltijdenOpBalie.remove(0);
        }
        return maaltijd;
    }

    /**
     * Vraag aantal maaltijden op dat op de balie staat
     *
     * @return aantal maaltijden op deze balie
     */
    public int aantalMaaltijden() {
        return maaltijdenOpBalie.size();
    }
}