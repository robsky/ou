package nl.gezelligdruk.restaurant;

import java.util.logging.Logger;

/**
 * The type Ober. Haalt {@link Maaltijd} van de {@link Uitgiftebalie} en brengt het naar de tafels
 */
class Ober implements Runnable {

    private static final int WACHTTIJD = 1000;
    private static final int LOOPTIJD = 500;
    private final String naam;
    private final Uitgiftebalie uitgiftebalie;
    private static boolean werken = true;
    private static final Logger LOGGER = Logger.getLogger(Ober.class.getName());

    /**
     * Constructor voor een nieuwe Ober.
     *
     * @param naam naam van de ober
     * @param uitgiftebalie uitgiftebalie waar de ober de gerechten ophaalt
     */
    public Ober(String naam, Uitgiftebalie uitgiftebalie) {
        this.naam = naam;
        this.uitgiftebalie = uitgiftebalie;
    }

    /**
     * Werken.
     */
    public void werken() {
        werken = true;
    }

    /**
     * Stop werken.
     */
    public void stoppen() {
        werken = false;
        LOGGER.info(naam + " gaat stoppen");
    }

    /**
     * pak maaktijd en serveer deze uit
     *
     * @param maaltijd De maaltijd die uitgeserveerd moet worden.
     */
    private void serveer(Maaltijd maaltijd) {

        try {
            LOGGER.info(naam + " loopt naar tafel " + maaltijd.tafelnummer);
            Thread.sleep(LOOPTIJD * maaltijd.tafelnummer);
            LOGGER.info(naam + " serveert " + maaltijd.toString());
            LOGGER.info(naam + " loopt naar balie");
            Thread.sleep(LOOPTIJD * maaltijd.tafelnummer);
        } catch (InterruptedException e) {
            LOGGER.severe(e.getMessage() + " naam " + " is afgebroken");
        }
    }

    @Override
    public String toString() {
        return naam;
    }

    @Override
    public void run() {
        try {
            while (werken) {
                Maaltijd maaltijd = uitgiftebalie.pakMaaltijd();
                if (maaltijd != null) {
                    serveer(maaltijd);
                } else {
                    LOGGER.info(naam + " wacht");
                    Thread.sleep(WACHTTIJD);
                }
            }
        } catch (InterruptedException e) {
            LOGGER.severe(e.getMessage() + " " + naam + " is onderbroken");
        }
    }
}

