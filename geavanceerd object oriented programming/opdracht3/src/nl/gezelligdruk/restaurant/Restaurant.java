package nl.gezelligdruk.restaurant;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * The type Restaurant. Simulatie van een restaurant. Hier koken {@link Kok} {@link Maaltijd}. Plaatsen deze op de {@link Uitgiftebalie} voor de
 * {@link Ober} om uit te serveren aan de klanten aan tafels.
 */
class Restaurant {

    private Uitgiftebalie uitgiftebalie;

    // Logging format instellen
    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT [%4$s] %2$s: %5$s%n");
    }
    private static final Logger LOGGER = Logger.getLogger(Restaurant.class.getName());

    /**
     * The constant AANTALTAFELS. Aantal tafels in dit restaurant
     */
    public static final int AANTALTAFELS = 10;
    /**
     * Constante SIMULATIETIJD. Duur van simulatie
     */
    private static final int SIMULATIETIJD = 120000;

    /**
     * Instantiates a new Restaurant.
     */
    private Restaurant() {
        uitgiftebalie = new Uitgiftebalie();
    }

    /**
     * The entry point of application. Start de simulatie met 3 koks en 2 obers.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        Restaurant restaurant = new Restaurant();
        List threads = new ArrayList();

        Kok kok1 = new Kok("Kok 1", restaurant.uitgiftebalie);
        Kok kok2 = new Kok("Kok 2", restaurant.uitgiftebalie);
        Kok kok3 = new Kok("Kok 3", restaurant.uitgiftebalie);

        Thread threadKok1 = new Thread(kok1);
        Thread threadKok2 = new Thread(kok2);
        Thread threadKok3 = new Thread(kok3);
        threads.add(threadKok1);
        threads.add(threadKok2);
        threads.add(threadKok3);

        Ober ober1 = new Ober("Ober 1", restaurant.uitgiftebalie);
        Ober ober2 = new Ober("Ober 2", restaurant.uitgiftebalie);

        Thread threadOber1 = new Thread(ober1);
        Thread threadOber2 = new Thread(ober2);
        threads.add(threadOber1);
        threads.add(threadOber2);

        try {
            LOGGER.info("Starten simulatie");
            threadKok1.start();
            threadKok2.start();
            threadKok3.start();
            threadOber1.start();
            threadOber2.start();
            Thread.sleep(SIMULATIETIJD);
            LOGGER.info("Eindigen simulatie");
            kok1.stoppen();
            kok2.stoppen();
            kok3.stoppen();
            ober1.stoppen();
            ober2.stoppen();

            // Wachten tot alle treads klaar zijn. Hoeft niet, maar wel zo netjes.
            for (Object thread : threads) {
                ((Thread) thread).join();
            }

            LOGGER.info("Programma beendigd, met " + restaurant.uitgiftebalie.aantalMaaltijden() + " maaltijden op de balie.");
        } catch (InterruptedException e) {
            LOGGER.warning("Programma is onderbroken");
        }
    }

    @Override
    public String toString() {
        return "Restaurant{}";
    }
}
