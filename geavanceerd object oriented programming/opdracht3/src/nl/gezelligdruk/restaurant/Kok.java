package nl.gezelligdruk.restaurant;

import java.util.Random;
import java.util.logging.Logger;

/**
 * The type Kok. Hard werkende kok die continue kookt en {@link Maaltijd} op de {@link Uitgiftebalie} plaatst
 */
class Kok implements Runnable {
    private String naam;
    private static final long BEREIDINGSTIJD = 4000;
    private Uitgiftebalie uitgiftebalie;
    private volatile static boolean werken = true;
    private static final Logger LOGGER = Logger.getLogger(Kok.class.getName());

    /**
     * Kookboek van de kok. Bediscussieerbaar dat dit ook een propertie van het restuarant kan zijn. Maar aangezien de
     * gasten niet kunnen bestellen en de kok willekeurig mag kiezen uit het kookboek staat het nu hier.
     */
    private static final String[] GERECHT = new String[8];
    static {
        GERECHT[0] = "Pasta";
        GERECHT[1] = "Soep";
        GERECHT[2] = "Pannenkoek";
        GERECHT[3] = "Stoofpot";
        GERECHT[4] = "Salade";
        GERECHT[5] = "Toetje";
        GERECHT[6] = "Broodje";
        GERECHT[7] = "Omeletje";
    }

    /**
     * en voor de lol een smaakmaker om aan een gerecht toe te voegen.
     */
    private static final String[] SMAAKMAKER = new String[8];
    static {
        SMAAKMAKER[0] = "met Kaas";
        SMAAKMAKER[1] = "a la Mama";
        SMAAKMAKER[2] = "Slagroom";
        SMAAKMAKER[3] = "Glutenvrij";
        SMAAKMAKER[4] = "met Vis";
        SMAAKMAKER[5] = "Pesto";
        SMAAKMAKER[6] = "Omgekeerd";
        SMAAKMAKER[7] = "de la Casa";
    }

    /**
     * Instantiates een nieuwe Kok.
     *
     * @param naam naam van de kok
     * @param uitgiftebalie de uitgiftebalie waar de kok zijn gerechten aflevert
     */
    public Kok(String naam, Uitgiftebalie uitgiftebalie) {
        this.naam = naam;
        this.uitgiftebalie = uitgiftebalie;
    }

    /**
     * Stopt de kok met werken
     */
    public void stoppen() {
        werken = false;
        LOGGER.info(naam + " gaat stoppen");
    }

    /**
     * Werken.
     *
     * @throws InterruptedException the interrupted exception
     */
    public void werken() throws InterruptedException {
        werken = true;
        kook();
        LOGGER.info(naam + " is gestart");
    }

    /**
     * Maak een maaltijd voor een willekeurige tafel in het restaurant. Duurt BEREIDINGSTIJD lang. Na afronden wordt de
     * maaltijd op de balie geplaatst.
     *
     * @throws InterruptedException wordt gegooid als de kok moet stoppen
     */
    private void kook() throws InterruptedException {
        while (werken) {
            int tafel = kiesTafel();
            String gerecht = kiesGerecht();
            Maaltijd maaltijd = new Maaltijd(gerecht, tafel);
            LOGGER.info(naam + " begint met koken");
            Thread.sleep(BEREIDINGSTIJD);
            LOGGER.info(naam + " klaar met koken");
            uitgiftebalie.plaatsMaaltijd(maaltijd);
        }
    }

    /**
     * Selecteer een willekeurige tafel
     *
     * @return willekeurig tafelnummer
     */
    private int kiesTafel() {
        return new Random().nextInt(Restaurant.AANTALTAFELS) + 1;
    }

    /**
     * Stel een willekeurig gerecht en smaakmaker samen om te koken
     *
     * @return de naam van het samengestelde gerecht
     */
    private String kiesGerecht() {
        return GERECHT[(new Random().nextInt(GERECHT.length))] + " " + SMAAKMAKER[(new Random().nextInt(SMAAKMAKER.length))];
    }

    @Override
    public String toString() {
        return naam;
    }

    @Override
    public void run() {
        while (werken) {
            try {
                kook();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
