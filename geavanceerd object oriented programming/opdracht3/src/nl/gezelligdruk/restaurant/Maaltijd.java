package nl.gezelligdruk.restaurant;

import java.util.logging.Logger;

/**
 * The type Maaltijd. Combinatie van een gerecht en tafel
 */
class Maaltijd {

    private static final Logger LOGGER = Logger.getLogger(Maaltijd.class.getName());
    private final String omschrijving;
    final int tafelnummer;

    /**
     * Aanmaken Maaltijd object
     *
     * @param omschrijving representatie van maaltijd
     * @param tafelnummer the tafelnummer
     */
    public Maaltijd(String omschrijving, int tafelnummer) {
        this.omschrijving = omschrijving;
        this.tafelnummer = tafelnummer;
    }

    @Override
    public String toString() {
        return omschrijving + " voor tafel " + tafelnummer;
    }
}
